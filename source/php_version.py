# -*- coding=UTF-8-*-
'''
6L+Z5piv56iL5bqP5piv5LiB54OB5YaZ55qE77yMUVEyNDIwNDk4NTI2
Created on 2018年10月29日11:25:50
PHP版本操作&安装函数 Q2420498526
@author: coding1618@gmail.com
'''
import os,sys

#打印info信息函数
def INFO(mes):
	# 绿色字体
	print("\033[1;32m  INFO: \033[0m"),
	printcn(mes)

#警告信息
def TIP(mes):
	#黄色字体
	print('\033[1;33m')
	print('*' * 50)
	printcn('Warning: '+mes)
	print('*' * 50)
	print('\033[0m')

#打印错误信息函数
def ERROR(mes):
	# 红色字体
	print('\033[1;31m')
	print('*' * 50)
	printcn('Error: '+mes)
	print('*' * 50)
	print('\033[0m')

#解决中文乱码函数
def printcn(msg):
    print(msg.decode('utf-8').encode(sys_encoding))

#菜单函数
def menu():
    printcn('===========|请选择安装的功能序号|==============')
    INFO('          1 -> Centos系统安装默认yum库中的PHP版本')
    INFO('          2 -> Centos系统卸载已经PHP版本')
    INFO('          3 -> Centos系统更新已有PHP版本到最新版本(这里最高版本7.2)')
    printcn('========|感谢你的使用！！祝你好运！！|===========')


#安装yum库的默认PHP版本(这里不确定库中的版本是多少，具体看安装成功之后的php -v)
def installDefault():
    TIP('开始执行安装1-->安装yum库默认php版本:')
    ok = os.system('sudo yum -y install php')
    if ok != 0 :
        ERROR('安装yum默认库中的PHP版本失败！请检测你的服务器网络或者是否已经安装yum！')
    else:
        INFO('yum库默认php版本安装成功！')    

#卸载默认版本
def uninstallDefault():
    TIP('开始执行卸载2-->卸载yum库默认php版本:')
    ok = os.system('yum remove php')
    if ok != 0:
        ERROR('移除默认版本失败')
    else:
        INFO('移除成功！')

#更新新版本
def update():
    TIP('开始执行更新3-->更新php版本到最新:')
    os.system('rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm')
    os.system('rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm')
    os.system('sudo yum -y install php72w')
    os.system('yum -y install php72w-cli php72w-common php72w-devel php72w-mysql')
    os.system('sudo yum -y install php72w-gd php72w-imap php72w-ldap php72w-odbc php72w-pear php72w-xml php72w-xmlrpc')
    ok = os.system('php -v')
    if ok != 0:
        ERROR('更新版本失败！')
    else:
        INFO('更新版本成功！')


if __name__ == '__main__':
	#获取系统编码
	syscode = '6L+Z5piv56iL5bqP5piv5LiB54OB5YaZ55qE77yMUVEyNDIwNDk4NTI2'
	sys_encoding = sys.getfilesystemencoding()
	#获取命令行参数
	try:
		c1 = sys.argv[1]
	except:
		menu()
	else:
		menu()
		if c1 is "1":
			installDefault()
		if c1 is "2":
			uninstallDefault()
		if c1 is "3":
			update()